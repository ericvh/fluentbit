FROM ubuntu:latest as builder

# Fluent Bit version
ENV FLB_MAJOR 1
ENV FLB_MINOR 1
ENV FLB_PATCH 2
ENV FLB_VERSION 1.1.2

ENV FLB_TAR https://fluentbit.io/releases/$FLB_MAJOR.$FLB_MINOR/fluent-bit-$FLB_VERSION.tar.gz

RUN mkdir -p /fluent-bit/bin /fluent-bit/etc
RUN  apt -q update && \
       apt install -yq \
       cmake \
       build-essential \
       make \
       flex \
       bison \
       openssl \
       git \
       wget \
    && cd /tmp && wget -q $FLB_TAR \
    && tar -xvzf fluent-bit-$FLB_VERSION.tar.gz \
    && cd "/tmp/fluent-bit-$FLB_VERSION"/build/ \
    && cmake -DFLB_DEBUG=On -DFLB_TRACE=On -DFLB_JEMALLOC=On -DFLB_BUFFERING=On ../ \
    && cd "/tmp/fluent-bit-$FLB_VERSION"/build/ \
    && make -j $(getconf _NPROCESSORS_ONLN)\
    && install -s bin/fluent-bit /fluent-bit/bin/ \
    && apt remove --purge -qq -y \
       cmake \
       flex \
       bison \
       openssl \
       wget \
    && rm -rf /tmp/*
RUN  mkdir -p /opt/archlib && (tar c /lib/*/libpthread* /lib/*/librt* /lib/*/libdl* /lib/*/libm* /lib/*/libgcc_s* /lib/*/libc* | (cd /opt/archlib && tar x))

FROM scratch

COPY --from=builder /opt/archlib/ /
COPY --from=builder /etc/ssl/certs /etc/ssl/certs
COPY --from=builder /fluent-bit /fluent-bit
ADD etc /fluent-bit/etc
# Entry point
CMD ["/fluent-bit/bin/fluent-bit", "-c", "/fluent-bit/etc/fluent-bit.conf"]